<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::paginate(25);

        return response()->json([
            'message' => 'Ürünler başarıyla listelendi',
            'data' => [
                'products' => $products
            ]
        ]);
    }

    public function show($product_id)
    {
        $product = Product::find($product_id);
        if (!$product) {
            return response()->json([
                'message' => 'Ürün bulunamadı',
                'data' => []
            ], 404);
        }

        $category = Category::find($product->category_id);
        return response()->json([
            'message' => 'Ürün başarıyla listelendi',
            'data' => [
                'category' => $category,
                'product' => $product
            ]
        ]);
    }
}
