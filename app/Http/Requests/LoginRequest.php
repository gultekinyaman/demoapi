<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'E-posta adresi gereklidir',
            'email.email' => 'E-posta adresi geçerli formatta değil',
            'password.required' => 'Parola gereklidir',
            'password.min' => 'Parola minimum 6 karakterden oluşmalıdır',
        ];
    }

}
